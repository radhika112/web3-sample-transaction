const MyContract = artifacts.require('MyContract.sol');

contract('MyContract', () => {
    it('Should update data', async () => {
        const storage =  await MyContract.new();
        await storage.updateData(10);
        const data = await storage.readData();
        assert(data.toString() === '10')
    });
});