const Web3 = require('web3');
const Provider = require('@truffle/hdwallet-provider');
const MyContract = require('./build/contracts/MyContract.json');
const address = '0x4A0526daeaE906a6F94550a0D4cE23984df9a465';
const privateKey =
  '40fdf18ea668403424381586483c98e61159c5249a8b49afe76fbca65d19a9d9';
const infuraUrl =
  'https://rinkeby.infura.io/v3/633196aec5ea4a5aba0b9ee875838a93';
const myAdd = '0x4A0526daeaE906a6F94550a0D4cE23984df9a465';

const init1 = async () => {
  const web3 = new Web3(
    new Web3.providers.HttpProvider(infuraUrl)
  );
  const networkId = await web3.eth.net.getId();
  console.log('networkId == ',networkId);
  const contract = new web3.eth.Contract(
    MyContract.abi,
    MyContract.networks[networkId].address,
  )
  const tx = contract.methods.updateData(5)
  const gas = await tx.estimateGas({ from: address })
  const gasPrice = await web3.eth.getGasPrice()
  const data = tx.encodeABI()
  const nonce = await web3.eth.getTransactionCount(address)
  const signedTx = await web3.eth.accounts.signTransaction(
    {
      to: contract.options.address,
      data,
      gas,
      gasPrice,
      nonce,
      chainId: networkId,
    },
    privateKey,
  );
  // console.log(`Old data value : ${await contract.methods.data().call()}`);
  const receipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
  console.log(`Transaction Hash  =  ${receipt.transactionHash}`);
  // console.log(`New data value : ${await contract.methods.data().call()}`);
}

init1()
