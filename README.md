## Third party URLs

1. Hit command ** npm install **
2. Get Faucet Address and Private key from **https://vanity-eth.tk/**
3. Use rinkeby public testnet to get some testnet eth.
4. Create project on **https://infura.io/** to generate mnemonic which is passed into the network providers

---

## Instructions to run Project

1. npm install
2. truffle compile
3. truffle migrate --reset --network rinkeby
4. node script.js => It'll console one **Transaction Hash**

---

## URL to check Transaction Details

Enter **Transaction Hash** to see transaction details.

**https://rinkeby.etherscan.io/** 